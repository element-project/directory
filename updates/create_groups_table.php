<?php

namespace Empu\Directory\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

/**
 * CreateGroupsTable Migration
 */
class CreateGroupsTable extends Migration
{
    public function up()
    {
        Schema::create('empu_directory_groups', function (Blueprint $table) {
            $table->tinyIncrements('id');
            $table->string('code')->unique();
            $table->string('label');
            $table->string('icon')->nullable();
            $table->unsignedTinyInteger('menu_order')->default(0);
            $table->boolean('is_system')->default(false);
            $table->boolean('has_restriction')->default(false);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('empu_directory_groups');
    }
}
