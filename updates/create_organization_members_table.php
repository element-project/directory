<?php namespace Empu\Directory\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

/**
 * CreateOrganizationMembersTable Migration
 */
class CreateOrganizationMembersTable extends Migration
{
    public function up()
    {
        Schema::create('empu_directory_organization_members', function (Blueprint $table) {
            $table->unsignedBigInteger('organization_id');
            $table->foreign('organization_id', 'organization_members_fk1')
                ->references('id')
                ->on('empu_directory_parties')
                ->onDelete('cascade');
            $table->unsignedBigInteger('member_id');
            $table->foreign('member_id', 'organization_members_fk2')
                ->references('id')
                ->on('empu_directory_parties')
                ->onDelete('cascade');
            $table->primary(['organization_id', 'member_id'], 'organization_members_pk');
        });
    }

    public function down()
    {
        Schema::dropIfExists('empu_directory_organization_members');
    }
}
