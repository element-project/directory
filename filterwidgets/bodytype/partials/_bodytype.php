<a
    href="javascript:;"
    class="filter-scope <?= $value ? 'active' : '' ?>"
    data-scope-name="<?= $name ?>"
>
    <span class="filter-label">
        <?= e(trans($scope->label)) ?><?= $value ? ": {$selectedOption}" : '' ?>
    </span>
</a>
