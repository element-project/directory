<?php

namespace Empu\Directory\Contracts;

interface SubtypeOfParty
{
    public function partyRelationName(): string;
}