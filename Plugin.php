<?php

namespace Empu\Directory;

use Backend;
use Empu\Directory\Models\DirectorySetting;
use Empu\Directory\Models\Group;
use Event;
use System\Classes\PluginBase;

/**
 * Directory Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Directory',
            'description' => 'No description provided yet...',
            'author'      => 'Wuri Nugrahadi',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
        Event::listen('backend.menu.extendItems', Listeners\RegisterDirectoryMenu::class);
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        $permissions = [
            'empu.directory.access_contacts' => [
                'tab' => 'Directory',
                'label' => 'Access contacts'
            ],
            'empu.directory.manage_groups' => [
                'tab' => 'Directory',
                'label' => 'Manage groups'
            ],
            'empu.directory.manage_settings' => [
                'tab' => 'Directory',
                'label' => 'Manage Core settings'
            ],
        ];

        foreach (Group::hasRestrictions()->get() as $group) {
            $permissions["empu.directory.access_contacts.{$group->code}"] = [
                'tab' => 'Directory',
                'label' => "Access contacts of {$group->label} group",
            ];
        }

        return $permissions;
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return [
            'directory' => [
                'label' => 'Directory',
                'url'         => Backend::url('empu/directory/contacts'),
                'icon'        => 'icon-address-book-o',
                'permissions' => ['empu.directory.access_contacts'],
                'order'       => 300,
                'sideMenu' => [
                    'contacts' => [
                        'label' => 'All Contacts',
                        'url' => Backend::url('empu/directory/contacts'),
                        'icon' => 'icon-address-book-o',
                        'permissions' => ['empu.directory.access_contacts'],
                        'order' => 301,
                    ],
                ],
            ],
        ];
    }

    public function registerSettings()
    {
        return [
            'groups' => [
                'label' => 'Groups',
                'description' => 'Manage available groups.',
                'category' => 'Directory',
                'icon' => 'icon-globe',
                'url' => Backend::url('empu/directory/groups'),
                'order' => 500,
                'keywords' => 'directory group contact',
                'permissions' => ['empu.directory.manage_groups'],
            ],
            'settings' => [
                'label' => 'Directory Settings',
                'description' => 'Manage director settings.',
                'category' => 'Directory',
                'icon' => 'icon-cog',
                'class' => DirectorySetting::class,
                'order' => 500,
                'keywords' => 'crm erp directory organizaton',
                'permissions' => ['empu.directory.manage_settings']
            ]
        ];
    }

    public function registerFilterWidgets()
    {
        return [
            \Empu\Directory\FilterWidgets\BodyType::class => 'bodytype',
        ];
    }
}
