<?php

namespace Empu\Directory\Listeners;

use Backend\Facades\Backend;
use Empu\Directory\Models\Group;

class RegisterDirectoryMenu
{
    public function handle($manager)
    {
        $subMenu = Group::all()
            ->mapWithKeys(function ($type) {
                $menuKey = sprintf('distinctcontacts_%s', $type->code);
                $permissions = $type->has_restriction
                    ? ['empu.directory.access_contacts.' . $type->code]
                    : ['empu.directory.access_contacts'];

                return [
                    $menuKey => [
                        'label' => $type->label,
                        'url' => sprintf('%s?type=%s', Backend::url('empu/directory/distinctcontacts'), $type->code),
                        'icon' => $type->icon ?: 'icon-address-book',
                        'permissions' => $permissions,
                        'order' => 310 + ($type->menu_order ?: ($type->id + 256)),
                    ]
                ];
            });

        $manager->addSideMenuItems('Empu.Directory', 'directory', $subMenu->toArray());
    }
}