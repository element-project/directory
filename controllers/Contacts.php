<?php

namespace Empu\Directory\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Backend\Widgets\Form;
use BackendAuth;
use Empu\Directory\Models\Group;
use Illuminate\Contracts\Database\Eloquent\Builder;

/**
 * Contacts Backend Controller
 */
class Contacts extends Controller
{
    public $implement = [
        \Backend\Behaviors\FormController::class,
        \Backend\Behaviors\ListController::class
    ];

    public $requiredPermissions = ['empu.directory.access_contacts'];

    /**
     * @var string formConfig file
     */
    public $formConfig = 'config_form.yaml';

    /**
     * @var string listConfig file
     */
    public $listConfig = 'config_list.yaml';

    protected $hasGrouping = false;

    /**
     * __construct the controller
     */
    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Empu.Directory', 'directory', 'contacts');

        $this->hasGrouping = Group::count() > 0;
    }

    public function listExtendQuery(Builder $query): void
    {
        $includeGroups = [];
        foreach (Group::hasRestrictions()->get() as $group) {
            if (BackendAuth::userHasAccess("empu.directory.access_contacts.{$group->code}")) {
                array_push($includeGroups, $group->code);
            }
        }

        $query->with('groups')->accessible($includeGroups);
    }

    public function formExtendFields(Form $form): void
    {
        if (!$this->hasGrouping) {
            $form->removeField('groups');
        }
    }
}
