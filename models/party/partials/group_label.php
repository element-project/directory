<?php if ($record->groups->count()): ?>
    <?= $record->groups->pluck('label')->join(', ', ' &amp; ') ?>
<?php else: ?>
    <em class="text-muted">unlisted</em>
<?php endif ?>