<?php

namespace Empu\Directory\Models;

use Model;

class DirectorySetting extends Model
{
    /**
     * @var array implement these behaviors
     */
    public $implement = [
        \System\Behaviors\SettingsModel::class
    ];

    /**
     * @var string settingsCode unique to this model
     */
    public $settingsCode = 'empu_directory_settings';

    /**
     * @var string settingsFields configuration
     */
    public $settingsFields = 'fields.yaml';

    public $belongsTo = [
        'primary_organization' => [
            Party::class,
            'key' => 'primary_organization_id',
            'scope' => 'organizationOnly',
        ],
    ];
}