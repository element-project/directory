<?php

namespace Empu\Directory\Models;

use October\Rain\Database\Builder;
use October\Rain\Database\Model;

/**
 * Group Model
 */
class Group extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string table associated with the model
     */
    public $table = 'empu_directory_groups';

    /**
     * @var array guarded attributes aren't mass assignable
     */
    protected $guarded = ['*'];

    /**
     * @var array fillable attributes are mass assignable
     */
    protected $fillable = ['code', 'label'];

    /**
     * @var array rules for validation
     */
    public $rules = [
        'code' => 'required',
        'label' => 'required',
    ];

    /**
     * @var array Attributes to be cast to native types
     */
    protected $casts = [];

    /**
     * @var array jsonable attribute names that are json encoded and decoded from the database
     */
    protected $jsonable = [];

    /**
     * @var array appends attributes to the API representation of the model (ex. toArray())
     */
    protected $appends = [];

    /**
     * @var array hidden attributes removed from the API representation of the model (ex. toArray())
     */
    protected $hidden = [];

    /**
     * @var array dates attributes that should be mutated to dates
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * @var array hasOne and other relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public function scopeHasRestrictions(Builder $builder, bool $boolean = true, array $includes = []): Builder
    {
        return $builder->where(function (Builder $q) use ($boolean, $includes) {
            $q->where($this->qualifyColumn('has_restriction'), $boolean)
                ->orWhereIn($this->qualifyColumn('code'), $includes);
        });
    }

    public function scopeHasNoRestrictions(Builder $builder, array $includes = []): Builder
    {
        return $this->scopeHasRestrictions($builder, false, $includes);
    }

    public function scopeApplyCode(Builder $builder, string $code): Builder
    {
        return $builder->where($this->qualifyColumn('code'), $code);
    }

    public static function findByCode(string $code): self
    {
        return self::make()->newModelQuery()->applyCode($code)->firstOrFail();
    }

    public static function seed(string $code, string $label): self
    {
        $contactType = self::applyCode($code)->first();

        if (!$contactType) {
            $contactType = self::create(compact('code', 'label'));
        }

        return $contactType;
    }
}
